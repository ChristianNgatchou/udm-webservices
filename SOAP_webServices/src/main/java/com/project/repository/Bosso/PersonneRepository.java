package com.project.repository.Bosso;

import com.project.models.Personne;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface PersonneRepository
        extends JpaRepository<Personne, Long> {


    Optional<Personne> findById(Long id);

    Optional<Personne> findByPassword(String password);


}
