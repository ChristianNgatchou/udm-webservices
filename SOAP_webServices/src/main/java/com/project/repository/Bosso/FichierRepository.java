package com.project.repository.Bosso;

import com.project.models.FichierBD;
import org.springframework.data.jpa.repository.JpaRepository;

public interface FichierRepository extends JpaRepository<FichierBD, String> {
}
