package com.project.repository.Meppa;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;

import com.project.models.Filiere;

public interface FiliereDao extends JpaRepository<Filiere, Long>{
    Optional<Filiere> findByName(String name);
}
