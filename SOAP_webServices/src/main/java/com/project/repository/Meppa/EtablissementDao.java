package com.project.repository.Meppa;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.project.models.Etablissement;

@Repository
public interface EtablissementDao extends JpaRepository<Etablissement, Long> {
    Optional<Etablissement> findByName(String nom);
}
