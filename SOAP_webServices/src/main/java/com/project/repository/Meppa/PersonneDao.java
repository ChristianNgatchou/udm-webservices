package com.project.repository.Meppa;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.project.models.Personne;

@Repository
public interface PersonneDao extends JpaRepository<Personne, Long> {
    
    Optional<Personne> findByMatricule(String matricule);
}
