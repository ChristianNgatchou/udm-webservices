package com.project.Exceptions.Meppa;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.NOT_FOUND)
public class FiliereIntrouvableException extends RuntimeException {

    public FiliereIntrouvableException(String arg0) {
        super(arg0);
    }
    
}
