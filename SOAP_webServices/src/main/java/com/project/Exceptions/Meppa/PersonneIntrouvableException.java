package com.project.Exceptions.Meppa;

import org.springframework.ws.soap.server.endpoint.annotation.FaultCode;
import org.springframework.ws.soap.server.endpoint.annotation.SoapFault;

@SoapFault(faultCode = FaultCode.CUSTOM,
        customFaultCode = "{" + PersonneIntrouvableException.NAMESPACE_URI + "}custom_fault",
        faultStringOrReason = "@faultString")
public class PersonneIntrouvableException extends RuntimeException {

	private static final long serialVersionUID = 1L;
	public static final String NAMESPACE_URI = "http://0.0.0.0/exception";

    public PersonneIntrouvableException(String message) {
        super(message);
    }
}
