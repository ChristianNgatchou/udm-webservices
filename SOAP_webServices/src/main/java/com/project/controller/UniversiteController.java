package com.project.controller;

import java.util.Date;
import java.util.Collection;
import java.util.List;
import java.util.Optional;

import com.project.services.Bosso.PasswordUpdateService;
import com.project.services.Bosso.PersonneService_Bosso;
import org.springframework.beans.factory.annotation.Autowired;

import com.project.Exceptions.Meppa.PersonneIntrouvableException;
import com.project.models.Filiere;
import com.project.models.Personne;
import com.project.models.Planning;
import com.project.models.Role;
import com.project.services.Meppa.EtablissemmentService;
import com.project.services.Meppa.FiliereService;
import com.project.services.Meppa.PersonneService;
import com.project.services.Ngatchou.PlanningService;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;

import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.PathVariable;

@Component
@WebService(serviceName = "soap")
public class UniversiteController {
    @Autowired
    EtablissemmentService etablissemmentService;

    @Autowired
    FiliereService filiereService;

    @Autowired
    PersonneService personneService;
    
    @Autowired
    PersonneService_Bosso personneService_bosso;

    @Autowired
    PasswordUpdateService passwordUpdateService;

    @Autowired
    PlanningService planningService;



    /******************************************************MEPPA*******************************************************************/
    
    /** 
     * @param nomEtablissement
     * @return List<Filiere>
     */
    @WebMethod
    public List<Filiere> getFilieresOfEtablissement(@WebParam(name = "nomEtablissement") String nomEtablissement) {
       List<Filiere> filieres = etablissemmentService.getFilieres(nomEtablissement);

       for (Filiere filiere : filieres) {
            filiere.setId(null);
            filiere.setEtablissement(null);
            filiere.setPersonnes(null);
       }

        return filieres;
    }

    /** 
     * @return List<Filiere>
     */
    @WebMethod
    public List<Filiere> getAllFilieres() {
       List<Filiere> filieres = filiereService.getFilieres();

       for (Filiere filiere : filieres) {
            filiere.setId(null);
        }

        return filieres;
    }

    
    /** 
     * @param nomFiliere
     * @return List<Personne>
     */
    @WebMethod
    public List<Personne> getPersonnelOfFiliere(@WebParam(name = "nomFiliere") String nomFiliere) {
       List<Personne> personnes = filiereService.getPersonnel(nomFiliere);

       for (Personne personne : personnes) {
            personne.setId(null);
            personne.setPassword(null);
            personne.setProfil(null);
            personne.setFiliere(null);
            
            Collection<Role> roles = personne.getRoles();

            for (Role role : roles) {
                role.setId(null);
            }
        }
        return personnes;
    }

    
    /** 
     * @param filiere
     * @return Filiere
     */
    @WebMethod
    public Filiere createFiliere(Filiere filiere) {
        
        Filiere filiereEnregistre = filiereService.saveOrUpdateFiliere(filiere);
        return filiereEnregistre;
    }

    /** 
     * @apiNote Cet API permet de recuperer les informations d'un membre du personnel par matricule
     * @param matricule
     * @return Personne
     * @throws PersonneIntrouvableException
     */
    @WebMethod
    public Personne getPersonneByMatricule(@WebParam(name = "matricule") String matricule){
       Optional<Personne> personne = personneService.getPersonne(matricule);

        if(!personne.isPresent()) {
            throw new PersonneIntrouvableException("Le membre du personnel avec le matricule " + matricule + " n'existe pas.");
            //return ResponseEntity.notFound().build();
        }

        personne.get().setId(null);
        personne.get().setPassword(null);
        personne.get().setProfil(null);

        return personne.get();

    }
    /*********************************************************************************************************************************/


    /******************************************************BOSSO***********************************************************************/
    /**
     * Cette methode permet de recuperer une personne a partir de son id
     */
    @WebMethod
    public Personne getPersonneById (@WebParam(name = "id") long id){
        return personneService_bosso.getPersonById(id)
                .orElse(null);
    }

    /**
     * Cette methode permet de recuperer une personne a partir de son mot de passe
     */
    @WebMethod
    public Personne getPersonneByPassword (@PathVariable(value = "password") String password){
        return personneService_bosso.getPersonByPassword(password)
                .orElse(null);
    }

    /**
     * Cette methode permet de modifier le mot de passe d'un utilisateur
     */
    @WebMethod
    public Personne updatePersonnePassword (@WebParam(name = "id") long id, @WebParam(name = "newPassword") String newPassword){
        passwordUpdateService.updatePassword(id, newPassword);
         return getPersonneById(id);

    }

    /*********************************************************************************************************************************/

    /******************************************************NGATCHOU*******************************************************************/

    @WebMethod
    public List<Planning> getAllPlanning(){
        return planningService.getAllPlannings();
    }
    
    @WebMethod
    public Planning savePlanning(Planning planning){
        return planningService.savePlanning(planning);
    }

    @WebMethod
    public List<Planning> getWeeklyPlanning(@WebParam(name = "date") Date date){
        return planningService.getPlannings(date);
    }

    @WebMethod
    public List<Planning> getPlanning(@WebParam(name = "date") Date date){
        return planningService.getPlanning(date);
    }

    /******************************************************************************************************************************/
}
