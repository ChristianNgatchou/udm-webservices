package com.project.models;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinTable;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

import org.hibernate.annotations.LazyCollection;
import org.hibernate.annotations.LazyCollectionOption;

@XmlRootElement
@Entity 
@Data 
@AllArgsConstructor 
@NoArgsConstructor
public class Etablissement {
    @Id 
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;
    private String name;
    @ManyToMany(
        fetch = FetchType.LAZY,
        cascade = { 
            CascadeType.PERSIST, 
            CascadeType.MERGE 
        }	
	)
	@JoinTable(
        name = "etablissement_campus",
        joinColumns = @JoinColumn(name = "etablissement_id"), 	
        inverseJoinColumns = @JoinColumn(name = "campus_id")
	)
    @XmlTransient
    private List<Campus> campus = new ArrayList<>();
    
    @OneToMany
    @LazyCollection(LazyCollectionOption.FALSE)
    @XmlTransient	
    private List<Filiere> filieres = new ArrayList<>();
}
