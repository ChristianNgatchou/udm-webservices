package com.project.models;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToOne;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

@XmlRootElement
@Entity 
@Data 
@AllArgsConstructor 
@NoArgsConstructor
public class Classe {
    @Id 
    @GeneratedValue(strategy = GenerationType.AUTO  )
    private Long id;
    @OneToOne(fetch = FetchType.EAGER)
    @XmlTransient
    private Filiere filiere;
    @OneToOne(fetch = FetchType.EAGER)
    @XmlTransient
    private Niveau niveau;
}
