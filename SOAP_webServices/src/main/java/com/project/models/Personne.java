package com.project.models;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.List;

import javax.persistence.*;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.LazyCollection;
import org.hibernate.annotations.LazyCollectionOption;

import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

@XmlRootElement
@Entity 
@Data 
@AllArgsConstructor 
@NoArgsConstructor
public class Personne {
    @Id 
    @GeneratedValue(strategy = GenerationType.AUTO  )
    private Long id;
    @Column(unique=true)
    private String matricule;
    private String firstname;
    private String lastname;
    private Date date_of_birth;
    private String password;
    private Byte profil;
    @ManyToOne(fetch = FetchType.EAGER)
    @XmlTransient
	private Filiere filiere;
    @ManyToMany
    @LazyCollection(LazyCollectionOption.FALSE)
    @XmlTransient
    private List<Role> roles = new ArrayList<>();
}
