package com.project.models;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;

import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@XmlRootElement
@Entity 
@Data 
@AllArgsConstructor 
@NoArgsConstructor
public class Campus {
    @Id 
    @GeneratedValue(strategy = GenerationType.AUTO )
    private Long id;
    private String name;
    @ManyToMany(fetch = FetchType.EAGER, mappedBy = "campus")
    @XmlTransient 
	private List<Etablissement> etablissements = new ArrayList<>();
}
