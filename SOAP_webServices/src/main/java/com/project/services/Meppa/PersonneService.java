package com.project.services.Meppa;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.project.models.Personne;
import com.project.repository.Meppa.PersonneDao;

@Service
public class PersonneService implements IPersonneService{
    @Autowired
    PersonneDao personneDao;

    @Override
    public Optional<Personne> getPersonne(String matricule) {
        Optional<Personne> personne = personneDao.findByMatricule(matricule);
        return personne;
    }
}
