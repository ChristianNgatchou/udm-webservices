package com.project.services.Meppa;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.project.Exceptions.Meppa.EtablissementIntrouvableException;
import com.project.Exceptions.Meppa.FiliereIntrouvableException;
import com.project.models.Etablissement;
import com.project.models.Filiere;
import com.project.repository.Meppa.EtablissementDao;

@Service
public class EtablissemmentService implements IEtablissementService {
    @Autowired
    EtablissementDao etablissementDao;

    @Override
    public Optional<Etablissement> getEtablissement(String nom) {
        Optional<Etablissement> etablissement = etablissementDao.findByName(nom);

        if(!etablissement.isPresent()){
            throw new EtablissementIntrouvableException("L'etablissement avec le nom " + nom + " est introuvable.");
        }
        
        return etablissement;
    }

    @Override
    public Optional<Etablissement> getEtablissement(Long id) {
        Optional<Etablissement> etablissement = etablissementDao.findById(id);

        if(!etablissement.isPresent()){
            throw new EtablissementIntrouvableException("L'etablissement avec l'id " + id + " est introuvable.");
        }
        
        return etablissement;
    }
   
    @Override
    public List<Filiere> getFilieres(String nomEtablissement) {
        Etablissement etablissement = getEtablissement(nomEtablissement).get();
        List<Filiere> filieres = etablissement.getFilieres();

        if(filieres.isEmpty()){
            throw new FiliereIntrouvableException("Aucune filiere n'a été trouvé dans l'etablissement " + nomEtablissement);
        }

        return filieres;
    }
 
}
