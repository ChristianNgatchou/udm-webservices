package com.project.services.Meppa;

import java.util.Optional;

import com.project.models.Personne;

public interface IPersonneService {
    Optional<Personne> getPersonne(String matricule);
}
