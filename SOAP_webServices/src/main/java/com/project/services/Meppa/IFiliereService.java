package com.project.services.Meppa;

import java.util.Optional;

import com.project.models.Filiere;
import com.project.models.Personne;

import java.util.List;

public interface IFiliereService {
    Optional<Filiere> getFiliere(String nom);
    Optional<Filiere> getFiliere(Long id);
    List<Filiere> getFilieres();
    List<Personne> getPersonnel(String nomFiliere);
    Filiere saveOrUpdateFiliere(Filiere filiere);
}
