package com.project.services.Meppa;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.project.Exceptions.Meppa.FiliereIntrouvableException;
import com.project.Exceptions.Meppa.PersonneIntrouvableException;
import com.project.models.Etablissement;
import com.project.models.Filiere;
import com.project.models.Personne;
import com.project.repository.Meppa.EtablissementDao;
import com.project.repository.Meppa.FiliereDao;

@Service
public class FiliereService implements IFiliereService{
    @Autowired
    FiliereDao filiereDao;

    @Autowired
    EtablissementDao etablissementDao;

    @Autowired
    IEtablissementService etablissementService;

    
    /** 
     * @param nom
     * @return Optional<Filiere>
     */
    @Override
    public Optional<Filiere> getFiliere(String nom) {
        Optional<Filiere> filiere = filiereDao.findByName(nom);

        if(!filiere.isPresent()){
            throw new FiliereIntrouvableException("La filiere avec le nom " + nom + " est introuvable.");
        }
        
        return filiere;
    }

    
    /** 
     * @param id
     * @return Optional<Filiere>
     */
    @Override
    public Optional<Filiere> getFiliere(Long id) {
        Optional<Filiere> filiere = filiereDao.findById(id);

        if(!filiere.isPresent()){
            throw new FiliereIntrouvableException("La filiere avec l'id " + id + " est introuvable.");
        }

        setNull(filiere.get());
        
        return filiere;
    }

    
    /** 
     * @param nomFiliere
     * @return List<Personne>
     */
    @Override
    public List<Personne> getPersonnel(String nomFiliere) {
        Optional<Filiere> filiere = getFiliere(nomFiliere);
        List<Personne> personnes = filiere.get().getPersonnes();

        if(personnes.isEmpty()){
            throw new PersonneIntrouvableException("Aucun membre du personnel n'a été trouvé travaillant dans la filiere " + nomFiliere);
        }

        return personnes;
    }

    
    /** 
     * @return List<Filiere>
     */
    @Override
    public List<Filiere> getFilieres() {
        List<Filiere> filieres = filiereDao.findAll();
        for (Filiere filiere : filieres) {
            setNull(filiere);
        }
        return filieres;
    }

    
    /** 
     * @param filiere
     * @return Filiere
     */
    @Override
    @Transactional(readOnly = false)
    public Filiere saveOrUpdateFiliere(Filiere filiere) {

        Filiere saveFiliere = filiereDao.save(filiere);
        Etablissement etablissement = etablissementDao.findById(filiere.getEtablissement().getId()).get();

        etablissement.getFilieres().add(saveFiliere);
        
        etablissementDao.save(etablissement);

        setNull(saveFiliere);

        return saveFiliere;
    }

    public void setNull(Filiere filiere){
        filiere.getEtablissement().setCampus(null);
        filiere.getEtablissement().setFilieres(null);
        filiere.setPersonnes(null);
    }
    
}
