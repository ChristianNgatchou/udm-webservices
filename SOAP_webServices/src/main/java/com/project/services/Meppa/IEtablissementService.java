package com.project.services.Meppa;

import java.util.List;
import java.util.Optional;

import com.project.models.Etablissement;
import com.project.models.Filiere;

public interface IEtablissementService {
    Optional<Etablissement> getEtablissement(String nom);
    Optional<Etablissement> getEtablissement(Long id);
    List<Filiere> getFilieres(String nomEtablissement);
}
