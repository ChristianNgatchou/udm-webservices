package com.project.services.Bosso;

import com.project.models.FichierBD;
import com.project.repository.Bosso.FichierRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.stream.Stream;

@Service
public class FichierSauvegardeService {

    @Autowired
    private FichierRepository fichierRepository;

    public FichierBD store(MultipartFile fichier) throws IOException{
        String fileName = StringUtils.cleanPath(fichier.getOriginalFilename());
        FichierBD fichierBD = new FichierBD(fileName, fichier.getContentType(), fichier.getBytes());

        return fichierRepository.save(fichierBD);

    }

    public FichierBD getFile(String id) {
        return fichierRepository.findById(id).get();
    }

    public Stream<FichierBD> getAllFiles() {
        return fichierRepository.findAll().stream();
    }
}
