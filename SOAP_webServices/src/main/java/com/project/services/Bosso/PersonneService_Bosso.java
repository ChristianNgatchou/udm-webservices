package com.project.services.Bosso;

import com.project.models.Personne;
import com.project.repository.Bosso.PersonneRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class PersonneService_Bosso {
    @Autowired
    private PersonneRepository personneRepository;

    public PersonneService_Bosso(PersonneRepository personneRepository){
        this.personneRepository = personneRepository;
    }

    public Optional<Personne> getPersonById(Long id){

        return personneRepository.findById(id);
    }

    public Optional<Personne> getPersonByPassword(String password){
        return personneRepository.findByPassword(password);
    }


}
