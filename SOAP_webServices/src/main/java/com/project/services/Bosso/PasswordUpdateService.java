package com.project.services.Bosso;

import com.project.models.Personne;
import com.project.repository.Bosso.PersonneRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class PasswordUpdateService {

    @Autowired
    private PersonneRepository personneRepository;


    public void updatePassword(Long id, String newPassword){

        Optional<Personne> existingPersonne = personneRepository.findById(id);

        if (existingPersonne.isPresent()) {
            Personne personne = existingPersonne.get();

            personne.setPassword(newPassword);
            personneRepository.save(personne);
        }
        else {
            System.out.println("**NO SUCH PERSON IN DB**");
        }



    }
}
