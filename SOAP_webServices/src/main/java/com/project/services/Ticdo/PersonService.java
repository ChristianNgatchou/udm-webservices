package com.project.services.Ticdo;

import com.project.models.Filiere;
import com.project.models.Personne;
import com.project.models.Role;
import com.project.repository.Ticdo.FiliereRepository;
import com.project.repository.Ticdo.PersonRepository;
import com.project.repository.Ticdo.RoleRepository;
import lombok.Data;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
@Data
public class PersonService {

    @Autowired
    private PersonRepository personRepository;
    @Autowired
    private RoleRepository roleRepository;
    @Autowired
    private FiliereRepository filiereRepository;

    public Optional<Personne> getPerson(final Long id) {
        return personRepository.findById(id);
    }

    public List<Personne> getPersons() {
        List<Personne> personnes = personRepository.findAll();
        for (Personne personne : personnes) {
            personne.setId(null);
        }
        return personnes;
    }


    public Personne savePerson(Personne person) {
        Personne savePerson = personRepository.save(person);
        Filiere filiere = filiereRepository.findById(person.getId()).get();
        filiere.getPersonnes().add(savePerson);
        filiereRepository.save(filiere);
        return savePerson;
    }

    public Personne addRoleToPerson(Long personId, Long roleId) {
        Personne personToSave = personRepository.findById(personId).get();
        Role savedRole = roleRepository.findById(roleId).get();
        personToSave.getRoles().add(savedRole);
        return personRepository.save(personToSave);
    }
}
