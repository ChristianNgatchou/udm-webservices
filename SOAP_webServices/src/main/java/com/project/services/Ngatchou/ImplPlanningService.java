package com.project.services.Ngatchou;

import java.util.Date;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.project.models.Planning;
import com.project.repository.Ngatchou.PlanningRepository;

@Service 
public class ImplPlanningService implements PlanningService{

    @Autowired
    private PlanningRepository planningRepo;

    @Override
    @Transactional(readOnly = false)
    public Planning savePlanning(Planning planning) {
        return planningRepo.save(planning);
    }

    @Override
    public List<Planning> getAllPlannings() {
        return planningRepo.findAll();
    }

    @Override
    public List<Planning> getPlannings(Date date) {
        List<java.util.Date> dateList = getAllDatesOfWeek(date);
        return planningRepo.findAllByDateIn(dateList);
    }

    public static List<java.util.Date> getAllDatesOfWeek(Date d1) { 
        List<java.util.Date> dates = new ArrayList<>();
        Calendar date1 = Calendar.getInstance();
        Calendar date2 = Calendar.getInstance();

        date1.setTime(d1);
        date2.setTime(d1);

        //ajout de la date de base
        dates.add(date2.getTime());

        //ici on va juste décrementer la date jusqu'a tomber sur le premier jour de la semaine.
        while (date1.get(Calendar.DAY_OF_WEEK) != Calendar.MONDAY) {
            date1.add(Calendar.DAY_OF_WEEK,-1);
            dates.add(date1.getTime());
        }

        //ici on va juste incrémenter la date jusqu'a tomber sur le dernier jour de la semaine.
        while (date2.get(Calendar.DAY_OF_WEEK) != Calendar.SUNDAY) {
            date2.add(Calendar.DAY_OF_WEEK,+1);
            dates.add(date2.getTime());
        }

        return dates;
    }

    @Override
    public List<Planning> getPlanning(Date date) {
        List<Planning> plannings = planningRepo.findAllByDate(date);
        if(plannings.isEmpty())
            return null;
        else
            return plannings;
    }
    
}

