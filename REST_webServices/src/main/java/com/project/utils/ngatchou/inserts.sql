    
    INSERT INTO campus(id, name) values(1, "Banekane");
    INSERT INTO etablissement(id, name) values(1, "FST");
    INSERT INTO etablissement_campus(etablissement_id, campus_id) values(1, 1);
    INSERT INTO filiere(id, name, etablissement_id) values(1, "GIS", 1);
    INSERT INTO filiere(id, name, etablissement_id) values(2, "GBM", 1);
    INSERT INTO niveau(id, value) values(1, 1);
    INSERT INTO niveau(id, value) values(2, 2);
    INSERT INTO niveau(id, value) values(3, 3);
    INSERT INTO classe(id, filiere_id, niveau_id) values(1, 1, 1);
    INSERT INTO classe(id, filiere_id, niveau_id) values(2, 2, 1);
    INSERT INTO personne(id, date_of_birth, firstname, lastname, matricule, password, filiere_id) 
        values(1, "1999-10-05", "ngatchou", "christian", "20c143", "root", 1);
    INSERT INTO personne(id, date_of_birth, firstname, lastname, matricule, password, filiere_id) 
        values(2, "1999-10-05", "tuekam", "carelle", "18C458", "root", 2);
    INSERT INTO cours(id, code, name, classe_id, enseignant_id) values(1, "INF401", "Atelier des microcontroleurs", 1, 1);
    INSERT INTO cours(id, code, name, classe_id, enseignant_id) values(2, "INF401", "Atelier des microcontroleurs", 2, 1);

# requete sql

#ajout d'un campus
    INSERT INTO campus(id, name) values(1, "Banekane");

#ajout d'un établissement
    INSERT INTO etablissement(id, name) values(1, "FST");

#ajout d'un établissement_campus
    INSERT INTO etablissement_campus(etablissement_id, campus_id) values(1, 1);

#ajout d'une filière
    INSERT INTO filiere(id, name, etablissement_id) values(1, "GIS", 1);
    INSERT INTO filiere(id, name, etablissement_id) values(2, "GBM", 1);

#ajout d'un niveau
    INSERT INTO niveau(id, value) values(1, 1);
    INSERT INTO niveau(id, value) values(2, 2);
    INSERT INTO niveau(id, value) values(3, 3);

#ajout d'une classe
    INSERT INTO classe(id, filiere_id, niveau_id) values(1, 1, 1);
    INSERT INTO classe(id, filiere_id, niveau_id) values(2, 2, 1);

#ajout d'une personne
    INSERT INTO personne(id, date_of_birth, firstname, lastname, matricule, password, filiere_id) 
        values(1, "1999-10-05", "ngatchou", "christian", "20c143", "root", 1);
    INSERT INTO personne(id, date_of_birth, firstname, lastname, matricule, password, filiere_id) 
        values(2, "1999-10-05", "tuekam", "carelle", "18C458", "root", 2);

#ajout d'un cours
    INSERT INTO cours(id, code, name, classe_id, enseignant_id) values(1, "INF401", "Atelier des microcontroleurs", 1, 1);
    INSERT INTO cours(id, code, name, classe_id, enseignant_id) values(2, "INF401", "Atelier des microcontroleurs", 2, 1);