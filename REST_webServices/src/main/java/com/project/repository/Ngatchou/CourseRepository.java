package com.project.repository.Ngatchou;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;

import com.project.models.Cours;

public interface CourseRepository extends JpaRepository<Cours, Long> {
    Optional<Cours>findById(Long id);
}
