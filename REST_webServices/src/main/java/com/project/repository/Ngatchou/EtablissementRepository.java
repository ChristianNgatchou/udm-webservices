package com.project.repository.Ngatchou;

import org.springframework.data.jpa.repository.JpaRepository;

import com.project.models.Etablissement;

public interface EtablissementRepository extends JpaRepository<Etablissement, Long> {
    
}

