package com.project.repository.Ngatchou;

import java.sql.Date;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.project.models.Planning;

public interface PlanningRepository extends JpaRepository<Planning, Long> {
    List<Planning> findAllByDate(Date date);
    List<Planning> findAllByDateIn(Iterable<java.util.Date> date);
}
