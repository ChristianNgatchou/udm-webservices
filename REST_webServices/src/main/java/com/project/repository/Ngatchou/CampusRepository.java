package com.project.repository.Ngatchou;

import org.springframework.data.jpa.repository.JpaRepository;

import com.project.models.Campus;

public interface CampusRepository extends JpaRepository<Campus, Long> {
}
