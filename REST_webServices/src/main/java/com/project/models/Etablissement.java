package com.project.models;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinTable;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Entity 
@Data 
@AllArgsConstructor 
@NoArgsConstructor
public class Etablissement {
    @Id 
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;
    private String name;
    @ManyToMany(
        fetch = FetchType.LAZY,
        cascade = { 
            CascadeType.PERSIST, 
            CascadeType.MERGE 
        }	
	)
	@JoinTable(
        name = "etablissement_campus",
        joinColumns = @JoinColumn(name = "etablissement_id"), 	
        inverseJoinColumns = @JoinColumn(name = "campus_id")
	)
    private List<Campus> campus = new ArrayList<>();
    
    @OneToMany(fetch = FetchType.LAZY)	
    private List<Filiere> filieres = new ArrayList<>();
}
