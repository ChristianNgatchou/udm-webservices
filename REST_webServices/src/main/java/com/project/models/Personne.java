package com.project.models;

import java.sql.Date;
import java.util.ArrayList;
import java.util.Collection;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Entity 
@Data 
@AllArgsConstructor 
@NoArgsConstructor
public class Personne {
    @Id 
    @GeneratedValue(strategy = GenerationType.AUTO  )
    private Long id;
    //TODO changer en attribut unique
    private String matricule;
    private String firstname;
    private String lastname;
    private Date date_of_birth;
    private String password;
    private Byte profil;
    @ManyToOne(fetch = FetchType.EAGER)
	private Filiere filiere;
    @OneToMany(fetch = FetchType.LAZY)
    private Collection<Role> roles = new ArrayList<>();
}
