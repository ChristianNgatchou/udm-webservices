package com.project.models;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinTable;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Entity
@Data
@AllArgsConstructor
@NoArgsConstructor
public class Filiere {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;
    private String name;
    @ManyToMany(
        fetch = FetchType.LAZY,
        cascade = { 
            CascadeType.PERSIST, 
            CascadeType.MERGE 
        }	
	)
	@JoinTable(
        name = "filiere_personne",
        joinColumns = @JoinColumn(name = "filiere_id"), 	
        inverseJoinColumns = @JoinColumn(name = "personne_id")
	)
    private List<Personne> personnes = new ArrayList<>();

    @ManyToOne(fetch = FetchType.EAGER)
	private Etablissement etablissement;
}
