package com.project.models;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Entity 
@Data 
@AllArgsConstructor 
@NoArgsConstructor
public class Cours {
    @Id 
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;
    //TODO changer en attribut unique
    private String code;
    private String name;
    @ManyToOne(fetch = FetchType.EAGER)	
    private Classe classe;
    @ManyToOne(fetch = FetchType.EAGER)
    private Personne enseignant;
}
