package com.project.services.Ngatchou;

import java.sql.Date;
import java.util.List;

import com.project.models.Planning;

public interface PlanningService {

    Planning savePlanning(Planning planning);
    List<Planning> getPlannings(Date date);
    List<Planning> getPlanning(Date date);
    List<Planning> getAllPlannings();

}
