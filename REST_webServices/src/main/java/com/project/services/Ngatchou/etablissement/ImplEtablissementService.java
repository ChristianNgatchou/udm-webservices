package com.project.services.Ngatchou.etablissement;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.project.models.Campus;
import com.project.models.Etablissement;
import com.project.repository.Ngatchou.CampusRepository;
import com.project.repository.Ngatchou.EtablissementRepository;

import lombok.RequiredArgsConstructor;

@Service 
@RequiredArgsConstructor 
@Transactional 
public class ImplEtablissementService implements EtablissementService{

    @Autowired
    private EtablissementRepository etablissementRepo;

    @Autowired
    private CampusRepository campusRepo;

    @Override
    public Etablissement saveEtablissement(Etablissement etablissement) {
        List<Campus> c = new ArrayList<>();
        for (Campus campus : etablissement.getCampus()) {
            c.add(campusRepo.findById(campus.getId()).get());
        }
        etablissement.setCampus(c);
        return etablissementRepo.save(etablissement);
    }

    @Override
    public Iterable<Etablissement> getEtablissements() {
        return etablissementRepo.findAll();
    }

    @Override
    public Optional<Etablissement> getEtablissement(Long id) {
        return etablissementRepo.findById(id);
    }
    
}
