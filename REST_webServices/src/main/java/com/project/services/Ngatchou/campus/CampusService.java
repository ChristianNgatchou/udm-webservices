package com.project.services.Ngatchou.campus;

import java.util.List;
import java.util.Optional;

import com.project.models.Campus;

public interface CampusService {

    Campus saveCampus(Campus campus);
    List<Campus> getCampus();
    Optional<Campus> getCampusById(Long id);

}
