package com.project.services.Ngatchou;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.project.models.Cours;
import com.project.repository.Ngatchou.CourseRepository;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;

@Service 
@RequiredArgsConstructor 
@Transactional 
@Slf4j
public class ImplCourseService implements CourseService{

    @Autowired
    private CourseRepository courseRepo;

    @Override
    public Cours saveCourse(Cours cours) {
        return courseRepo.save(cours);
    }

    @Override
    public List<Cours> getCourses() {
        return courseRepo.findAll();
    }

    @Override
    public Optional<Cours> getCourse(Long id) {
        log.info("id :{}",id);
        return courseRepo.findById(id);
    }
    
}

