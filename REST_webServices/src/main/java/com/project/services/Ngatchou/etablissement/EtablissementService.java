package com.project.services.Ngatchou.etablissement;

import java.util.Optional;

import com.project.models.Etablissement;

public interface EtablissementService {

    Etablissement saveEtablissement(Etablissement etablissement);
    Iterable<Etablissement> getEtablissements();
    Optional<Etablissement> getEtablissement(Long id);

}