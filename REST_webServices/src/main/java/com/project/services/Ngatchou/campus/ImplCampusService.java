package com.project.services.Ngatchou.campus;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.project.models.Campus;
import com.project.repository.Ngatchou.CampusRepository;

@Service 
public class ImplCampusService implements CampusService{
    
    @Autowired
    private CampusRepository campusRepository;

    @Override
    public Campus saveCampus(Campus campus) {
        return campusRepository.save(campus);
    }

    @Override
    public List<Campus> getCampus() {
        return campusRepository.findAll();
    }

    @Override
    public Optional<Campus> getCampusById(Long id) {
        return campusRepository.findById(id);
    }
    
}

