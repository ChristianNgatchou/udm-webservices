package com.project.services.Ngatchou;

import java.util.List;
import java.util.Optional;

import com.project.models.Cours;

public interface CourseService {

    Cours saveCourse(Cours cours);
    List<Cours> getCourses();
    Optional<Cours> getCourse(Long id);

}

