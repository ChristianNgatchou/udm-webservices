package com.project.controller.Ngatchou;

import java.net.URI;
import java.sql.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import com.project.models.Planning;
import com.project.services.Ngatchou.PlanningService;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;

@RestController
@RequestMapping("/api/rest/planning")
@RequiredArgsConstructor
@Slf4j
public class PlanningController {

    @Autowired
    private final PlanningService planningService;

    @GetMapping("/all")
    public ResponseEntity<List<Planning>> getAllPlanning(){
        return ResponseEntity.ok().body(planningService.getAllPlannings());
    }
    
    @PostMapping("/add")
    public ResponseEntity<Planning> savePlanning(@RequestBody Planning planning){

        log.info("Debug 0");
        URI uri = URI.create(ServletUriComponentsBuilder.fromCurrentContextPath().path("api/rest/planning/add").toUriString());
        return ResponseEntity.created(uri).body(planningService.savePlanning(planning));
    }

    @GetMapping("/weeklyPlanning/{date}")
    public ResponseEntity<List<Planning>>getWeeklyPlanning(@PathVariable Date date){
        return ResponseEntity.ok().body(planningService.getPlannings(date));
    }

    @GetMapping("/{date}")
    public ResponseEntity<List<Planning>>getPlanning(@PathVariable Date date){
        return ResponseEntity.ok().body(planningService.getPlanning(date));
    }
    
}
