package com.project.controller.Ngatchou;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.project.models.Etablissement;
import com.project.services.Ngatchou.etablissement.EtablissementService;

import io.swagger.v3.oas.annotations.parameters.RequestBody;
import lombok.RequiredArgsConstructor;

@RestController
@RequestMapping("/api/rest/etablissement")
@RequiredArgsConstructor
public class EtablissementController {
    @Autowired
    private final EtablissementService etablissementService;

    @GetMapping("/all")
    public Iterable<Etablissement> getAllEtablisssement(){
        return etablissementService.getEtablissements();
    }
    
    @PostMapping("/add")
    public Etablissement saveEtablissement(@RequestBody Etablissement  etablissement){
        System.out.println(etablissement.getId());
        return etablissementService.saveEtablissement(etablissement);
    }
    
}
