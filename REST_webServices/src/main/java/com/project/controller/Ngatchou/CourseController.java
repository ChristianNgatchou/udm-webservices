package com.project.controller.Ngatchou;

import java.net.URI;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import com.project.models.Cours;
import com.project.services.Ngatchou.CourseService;

import io.swagger.v3.oas.annotations.parameters.RequestBody;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;

@RestController
@RequestMapping("/api/rest/course")
@RequiredArgsConstructor
@Slf4j
public class CourseController {

    @Autowired
    private final CourseService courseService;

    @GetMapping("/all")
    public ResponseEntity<List<Cours>>getRoles(){
        return ResponseEntity.ok().body(courseService.getCourses());
    }
    
    @PostMapping("/add")
    public ResponseEntity<Cours>saveRole(@RequestBody Cours planning){
        return ResponseEntity.ok().body(courseService.saveCourse(planning));
    }
    
}

