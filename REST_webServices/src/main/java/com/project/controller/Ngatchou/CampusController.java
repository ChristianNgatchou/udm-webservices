package com.project.controller.Ngatchou;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.project.models.Campus;
import com.project.services.Ngatchou.campus.CampusService;

import io.swagger.v3.oas.annotations.parameters.RequestBody;
import lombok.RequiredArgsConstructor;

@RestController
@RequestMapping("/api/rest/campus")
public class CampusController {
    
    @Autowired
    CampusService campusService;

    @GetMapping("/all")
    public List<Campus>getAllCampus(){
        return campusService.getCampus();
    }
    
    @PostMapping("/add")
    public Campus saveCampus(@RequestBody Campus  campus){
        System.out.println(campus.getName());
        return campusService.saveCampus(campus);
    }
    
}
